select      distinct ClientId                       as 'ClientId'
            ,min(TransactionDate)                   as 'DataAtivacao'
into        #dataativ
from        Staging.Operacoes_Diaria                WITH(NOLOCK)
where       Product = 'CARTAO CREDITO'
and         Subproduct in ('FISICO CREDITO INTERNACIONAL','FISICO CREDITO NACIONAL','VIRTUAL CREDITO INTERNACIONAL','VIRTUAL CREDITO NACIONAL')
and         OperationStatus = 'EFETIVADO'
group by    ClientId
-----------------------------------------------------------------------------------------------------
select      Idcliente                               as 'ClientId'
            ,DataVencimentoFatura                   as 'DataFatura'
            ,RANK () OVER ( 
                            partition by idcliente
                            ORDER BY DataVencimentoReal
                          )                         as 'NumeroFatura' 
into        #datafaturas
FROM        NeonSRV.NeonPottencial.dbo.FaturaCredito as fc
inner join  #dataativ                               as da on da.ClientId = fc.IdCliente
where       fc.DataFechamento > da.DataAtivacao
-----------------------------------------------------------------------------------------------------
select      da.ClientId                             as 'ClientId'
            ,DataReferencia                         as 'DataFPD'
            ,DiasAtraso                             as 'Atraso'
            ,DataFatura                             as 'Data1Fatura'
into        #fpd
from        staging.bi_creditocontratocliente       as ccc WITH(NOLOCK)
inner join  #dataativ                               as da on da.ClientId = ccc.IdCliente
inner join  #datafaturas                            as df on df.ClientId = ccc.IdCliente
where       df.NumeroFatura = 1
and         DataReferencia = dateadd(day,5,df.DataFatura)
-----------------------------------------------------------------------------------------------------
select      da.ClientId                             as 'ClientId'
            ,DataReferencia                         as 'DAtaO30M2'
            ,DiasAtraso                             as 'Atraso'
            ,DataFatura                             as 'Data2Fatura'
            ,DataAtivacao
into        #o30m2
from        staging.bi_creditocontratocliente       as ccc with(nolock)
inner join  #dataativ                               as da on da.clientid = ccc.idcliente
inner join  #datafaturas                            as df on df.cleintid = ccc.idcliente
where       df.NumeroFatura = 2
and         DataReferencia = df.DataFatura
            
-----------------------------------------------------------------------------------------------------
select      da.CLientId                             as 'ClientId'
            ,DataReferencia                         as 'DataO30M3'
            ,DiasAtraso                             as 'Atraso'
            ,DataFatura                             as 'Data3Fatura'
            ,DataAtivacao
into        #o30m3
from        staging.bi_creditocontratocliente       as ccc WITH(NOLOCK)
inner join  #dataativ                               as da on da.ClientId = ccc.idcliente
inner join  #datafaturas                            as df on df.ClientId = ccc.IdCliente
where       df.NumeroFatura = 3
and         DataReferencia = df.DataFatura
-----------------------------------------------------------------------------------------------------
select      da.ClientId                             as 'ClientId'
            ,max(DiasAtraso)                        as 'AtrasoMax'
into        #atraso
from        Staging.BI_CreditoContratoCliente       as ccc
inner join  #dataativ                               as da on da.ClientId = ccc.idcliente
group by    da.ClientId
-----------------------------------------------------------------------------------------------------
select      distinct (da.ClientId)
            ,da.DataAtivacao
            ,case when fpd.Atraso>=5 then 1
                  else 0
                  end                               as 'FPD'
            ,fpd.Data1Fatura
            ,case when o3.Atraso>=30 then 'Over 30 mob 3'
                  else 'Adimplente'
                  end                               as 'Over 30 mob 3'
            ,o3.Data3Fatura
            ,case when o2.Atraso>=30 then 'Over 30 mob 2'
                  else 'Adimplente'
                  end                               as 'Over 30 mob 2'
from        #dataativ                               as da
left join   Dimension.Client                        as c WITH(NOLOCK) on da.ClientId = c.ClientId
left join   #fpd                                    as fpd on fpd.ClientId = c.ClientId
left join   #o30m3                                  as o3 on o3.ClientId = c.ClientId
left join   #o30m2                                  as o2 on o2.ClientId = c.ClientId
where       1=1
and         PersonType = 'PF'
and         FlagFraud = 0
and         CancellationRequestDate is null
and         EndDate is null
and         FlagRegisterDeleted = 0