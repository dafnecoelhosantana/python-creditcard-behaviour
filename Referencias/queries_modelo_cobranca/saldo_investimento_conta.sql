DECLARE @m0 DATETIME = '2020-01-31'
SELECT
    DISTINCT(c.ClientId)
    ,TotalBalanceCumulatedInvestAccount     AS 'VALUE_INVESTIMENT_BALANCE'
    ,TotalBalanceCumulatedCheckingAccount   AS 'VALUE_ACCOUNT_BALANCE'
	,cal.Date
FROM        NEONDW_BI.Fact.AccountBalance             AS ab WITH(NOLOCK)
INNER JOIN  NEONDW_BI.Dimension.Client                AS c WITH(NOLOCK) ON c.SKClient = ab.SKClient
INNER JOIN  NEONDW_BI.Dimension.Calendar              AS cal WITH(NOLOCK) ON cal.SKCalendar = ab.SKCalendar
inner join	(select distinct(ClientId) FROM NEONDW_BI.Staging.BI_RecuperacaoCobranca WITH(NOLOCK) where DiasAtraso >=3 group by ClientId) as rc ON rc.ClientId = c.ClientId
WHERE       cal.Date BETWEEN dateadd(month,-18,@m0) AND @m0