SELECT  ClientID, 
		DataFaturaInicial,
		MAX(CASE WHEN Tipo='ACORDO' THEN 1 ELSE 0 END) AS FlagAgreementDebt,
		SUM(CASE WHEN Tipo='ACORDO' THEN 1 ELSE 0 END) AS DaysAgreementDebt,

		MAX(DiasAtraso) AS DaysDelayedInvoice,--DiasAtrasoUteis2
		MAX(DiasAtrasoUteis2) AS DaysOfWeekDelayedInvoice,

		SUM(CASE WHEN TipoCliente='REGULAR' THEN 1 ELSE 0 END) AS DaysInvoiceNotDelayed,
		MAX(CASE WHEN TipoCliente='INADIMPLENTE' THEN 1 ELSE 0 END) AS FlagDefaulting ,
		MAX(CASE WHEN TipoCliente='DIVIDA' THEN 1 ELSE 0 END) AS FlagDebt,
		MAX(CASE WHEN TipoCliente='INADIMPLENTE' or TipoCliente='DIVIDA' THEN DataReferencia ELSE NULL END) as LastDelayedInvoideDay,

		MAX(ValorFaturaParcela) as PayedValueMax,
		MAX(ValorPagamentoMinimo) as MinPayedValueMax,
		MAX(Saldo) as BalanceMax,
		MAX(SaldoAjustado) as AdjustedBalanceMax,
		MAX(ValorPagoAcumuladoSemAntecipado) as CumulativeValuePayedWithoutAntecipationMax,
		MAX(ValorRecuperado) as RecoveredValueMax,
		MAX(SaldoPassado) as LastBalanceMax,

		MAX(ValorPagamentoMinimo/NULLIF(Saldo,0)) as MaxProporcionMinPayed,
		MIN(CASE WHEN ValorPagamentoMinimo/NULLIF(Saldo,0)>0 THEN ValorPagamentoMinimo/Saldo ELSE NULL END) as MinProportionMinPayed,
			   
		MAX(DataPagamento) as LastDayFlagPayment,
		SUM(CASE WHEN DataPagamento is not NULL THEN 1 ELSE 0 END) as CountPayments,
		SUM(ValorPago) as PayedValue,
		
		MAX(CASE WHEN StatusPagamento='PAGAMENTO ABAIXO DO MINIMO' THEN 1 ELSE 0 END) AS FlagStatusPayedLessThenMin,
		MAX(CASE WHEN StatusPagamento='PAGAMENTO ANTECIPADO' THEN 1 ELSE 0 END) AS FlagStatusInAdvancePayment,
		SUM(CASE WHEN StatusPagamento='PAGAMENTO ANTECIPADO' THEN 1 ELSE 0 END) AS StatusPagAntecipadoQuantidade,
		MAX(CASE WHEN StatusPagamento='PAGAMENTO APOS FECHAMENTO' THEN 1 ELSE 0 END) AS FlagStatusPayedBeforClosing,
		MAX(CASE WHEN StatusPagamento='PAGAMENTO DE PARCELAMENTO' THEN 1 ELSE 0 END) AS FlagStatusParcelingPayment,
		MAX(CASE WHEN StatusPagamento='PAGAMENTO MINIMO' THEN 1 ELSE 0 END) AS FlagStatusPayedLessThenMin,
		SUM(CASE WHEN StatusPagamento='PAGAMENTO MINIMO' THEN 1 ELSE 0 END) AS CountStatusLessThenMin,

		MAX(CASE WHEN MetodoPagamento='BOLETO' THEN 1 ELSE 0 END) AS FlagPeymentMethodBillet,
		MAX(CASE WHEN MetodoPagamento='DEBITO EM CONTA' THEN 1 ELSE 0 END) AS FlagPaymentMethodAccountDebiting,

		MAX(CASE WHEN TipoPagamento='PAGAMENTO REGULAR' THEN 1 ELSE 0 END) AS FlagPaymentTypeNotDelayed,
		MAX(CASE WHEN TipoPagamento='PAGAMENTO ACORDO' THEN 1 ELSE 0 END) AS FlagPaymentTypeAgreement,
		MAX(CASE WHEN TipoPagamento='ENTRADA PARCELAMENTO' THEN 1 ELSE 0 END) AS FlagPaymentTypeParceling


FROM        NEONDW_BI.Staging.BI_RecuperacaoCobranca       WITH(NOLOCK)
WHERE  ClientId in (select distinct(ClientId) FROM NEONDW_BI.Staging.BI_RecuperacaoCobranca WITH(NOLOCK) where DiasAtraso >=3 group by ClientId)

GROUP BY ClientID, DataFaturaInicial 
order by ClientID, DataFaturaInicial 