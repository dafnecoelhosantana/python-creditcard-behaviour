DECLARE @m0 DATETIME = '2020-01-31'
SELECT
    DISTINCT(ab.IdCliente)
    , LimiteCreditoTotal    AS 'VALUE_CREDIT_LIMIT'
    ,(case When LimiteCreditoTotal > LimiteCredito then (LimiteCreditoTotal-LimiteCredito)+LimiteCreditoDisponivel else LimiteCreditoDisponivel end)  AS 'VALUE_AVAILABLE_CREDIT_LIMIT'
	,ab.DataReferencia as 'DATE'
FROM        NEONDW_BI.Staging.BI_CreditoContratoCliente           AS ab WITH(NOLOCK)
inner join	(select distinct(ClientId) FROM NEONDW_BI.Staging.BI_RecuperacaoCobranca WITH(NOLOCK) where DiasAtraso >=3 group by ClientId) as rc ON rc.ClientId = ab.IdCliente
WHERE       ab.DataReferencia BETWEEN dateadd(month,-18,@m0) AND @m0


select top 1000 * from [NEONDW_BI].[Staging].[BI_CreditoContratoCliente] with (NOLOCK)