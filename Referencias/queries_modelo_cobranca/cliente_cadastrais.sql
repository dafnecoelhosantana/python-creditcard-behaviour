select	 
		PersonType
		,Gender
		--,Device  
		,Platform 
		,InstallSource
		,AddressPostalCode
		,Address
		,AddressNumber
		,AddressComplement
		,AddressNeighborhood
		,AddressCity
		,AddressState
		,Birthdate
		,ClientId
		,ClientRegisterId 
		,ApprovalDate
		--,DeclaredIncome 
		--,ProvenIncome 
		--,PresumedIncome 
		,RegisterSmallDate
		,FlagInvitedUser
		,FirstTransactionDate
		--,OriginType 
		,Source 

FROM	NEONDW_BI.Dimension.Client with (NOLOCK)
WHERE ClientId is not null
