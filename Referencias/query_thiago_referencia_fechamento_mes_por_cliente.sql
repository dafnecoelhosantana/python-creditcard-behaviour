
select		distinct IdCliente																																									as 'ClientId'
			,min(DataVencimentoAceite)																																							as 'DataContratacao'
into		#datacont
from		Staging.BI_CreditoContratoCliente WITH(NOLOCK)
where		datavencimentoaceite is not null
			and			StatusConta in ('Ativo','Cancelado','Bloqueado','Divida')
group by	IdCliente

-----------------------------------------------------------------------------------------------------

select		distinct ClientId																																									as 'ClientId'
			,min(TransactionDate)																																								as 'DataAtivacao'
into		#dataativ
from		Staging.Operacoes_Diaria WITH(NOLOCK)
where		Product = 'CARTAO CREDITO'
			and         Subproduct in ('FISICO CREDITO INTERNACIONAL','FISICO CREDITO NACIONAL','VIRTUAL CREDITO INTERNACIONAL','VIRTUAL CREDITO NACIONAL')
			and			OperationStatus = 'EFETIVADO'
group by	ClientId

-----------------------------------------------------------------------------------------------------

select		distinct(IdCliente) as 'ClientId'
			,max(case when year(ccc.datareferencia) = year(dataativacao) and month(ccc.datareferencia) = month(dataativacao) and datareferencia = eomonth(datareferencia) then DiasAtraso else null end) as 'AtrasoM0'
			,max(case when year(ccc.datareferencia) = year(dataativacao) and month(ccc.datareferencia) = month(dataativacao) and datareferencia = eomonth(datareferencia) then DataReferencia else null end) as 'DataM0'
			,max(case when year(ccc.datareferencia) = year(dataativacao) and month(ccc.datareferencia) = month(dataativacao) and datareferencia = eomonth(datareferencia) then (LimiteCredito-LimiteCreditoDisponivel) else null end) as 'SaldoM0'
			,max(case when year(ccc.datareferencia) = year(dataativacao) and month(ccc.datareferencia) = month(dataativacao) and datareferencia = eomonth(datareferencia) then LimiteCreditoTotal else null end) as 'LimiteM0'
			,max(case when year(ccc.datareferencia) = year(dateadd(month,1,dataativacao)) and month(ccc.datareferencia) = month(dateadd(month,1,dataativacao)) and datareferencia = eomonth(datareferencia) then DiasAtraso else null end) as 'AtrasoM1'
			,max(case when year(ccc.datareferencia) = year(dateadd(month,1,dataativacao)) and month(ccc.datareferencia) = month(dateadd(month,1,dataativacao)) and datareferencia = eomonth(datareferencia) then DataReferencia else null end) as 'DataM1'
			,max(case when year(ccc.datareferencia) = year(dateadd(month,1,dataativacao)) and month(ccc.datareferencia) = month(dateadd(month,1,dataativacao)) and datareferencia = eomonth(datareferencia) then (LimiteCredito-LimiteCreditoDisponivel) else null end) as 'SaldoM1'
			,max(case when year(ccc.datareferencia) = year(dateadd(month,1,dataativacao)) and month(ccc.datareferencia) = month(dateadd(month,1,dataativacao)) and datareferencia = eomonth(datareferencia) then LimiteCreditoTotal else null end) as 'LimiteM1'
			,max(case when year(ccc.datareferencia) = year(dateadd(month,2,dataativacao)) and month(ccc.datareferencia) = month(dateadd(month,2,dataativacao)) and datareferencia = eomonth(datareferencia) then DiasAtraso else null end) as 'AtrasoM2'
			,max(case when year(ccc.datareferencia) = year(dateadd(month,2,dataativacao)) and month(ccc.datareferencia) = month(dateadd(month,2,dataativacao)) and datareferencia = eomonth(datareferencia) then DataReferencia else null end) as 'DataM2'
			,max(case when year(ccc.datareferencia) = year(dateadd(month,2,dataativacao)) and month(ccc.datareferencia) = month(dateadd(month,2,dataativacao)) and datareferencia = eomonth(datareferencia) then (LimiteCredito-LimiteCreditoDisponivel) else null end) as 'SaldoM2'
			,max(case when year(ccc.datareferencia) = year(dateadd(month,2,dataativacao)) and month(ccc.datareferencia) = month(dateadd(month,2,dataativacao)) and datareferencia = eomonth(datareferencia) then LimiteCreditoTotal else null end) as 'LimiteM2'
			,max(case when year(ccc.datareferencia) = year(dateadd(month,3,dataativacao)) and month(ccc.datareferencia) = month(dateadd(month,3,dataativacao)) and datareferencia = eomonth(datareferencia) then DiasAtraso else null end) as 'AtrasoM3'
			,max(case when year(ccc.datareferencia) = year(dateadd(month,3,dataativacao)) and month(ccc.datareferencia) = month(dateadd(month,3,dataativacao)) and datareferencia = eomonth(datareferencia) then DataReferencia else null end) as 'DataM3'
			,max(case when year(ccc.datareferencia) = year(dateadd(month,3,dataativacao)) and month(ccc.datareferencia) = month(dateadd(month,3,dataativacao)) and datareferencia = eomonth(datareferencia) then (LimiteCredito-LimiteCreditoDisponivel) else null end) as 'SaldoM3'
			,max(case when year(ccc.datareferencia) = year(dateadd(month,3,dataativacao)) and month(ccc.datareferencia) = month(dateadd(month,3,dataativacao)) and datareferencia = eomonth(datareferencia) then LimiteCreditoTotal else null end) as 'LimiteM3'
			,max(case when year(ccc.datareferencia) = year(dateadd(month,4,dataativacao)) and month(ccc.datareferencia) = month(dateadd(month,4,dataativacao)) and datareferencia = eomonth(datareferencia) then DiasAtraso else null end) as 'AtrasoM4'
			,max(case when year(ccc.datareferencia) = year(dateadd(month,4,dataativacao)) and month(ccc.datareferencia) = month(dateadd(month,4,dataativacao)) and datareferencia = eomonth(datareferencia) then DataReferencia else null end) as 'DataM4'
			,max(case when year(ccc.datareferencia) = year(dateadd(month,4,dataativacao)) and month(ccc.datareferencia) = month(dateadd(month,4,dataativacao)) and datareferencia = eomonth(datareferencia) then (LimiteCredito-LimiteCreditoDisponivel) else null end) as 'SaldoM4'
			,max(case when year(ccc.datareferencia) = year(dateadd(month,4,dataativacao)) and month(ccc.datareferencia) = month(dateadd(month,4,dataativacao)) and datareferencia = eomonth(datareferencia) then LimiteCreditoTotal else null end) as 'LimiteM4'
			,max(case when year(ccc.datareferencia) = year(dateadd(month,5,dataativacao)) and month(ccc.datareferencia) = month(dateadd(month,5,dataativacao)) and datareferencia = eomonth(datareferencia) then DiasAtraso else null end) as 'AtrasoM5'
			,max(case when year(ccc.datareferencia) = year(dateadd(month,5,dataativacao)) and month(ccc.datareferencia) = month(dateadd(month,5,dataativacao)) and datareferencia = eomonth(datareferencia) then DataReferencia else null end) as 'DataM5'
			,max(case when year(ccc.datareferencia) = year(dateadd(month,5,dataativacao)) and month(ccc.datareferencia) = month(dateadd(month,5,dataativacao)) and datareferencia = eomonth(datareferencia) then (LimiteCredito-LimiteCreditoDisponivel) else null end) as 'SaldoM5'
			,max(case when year(ccc.datareferencia) = year(dateadd(month,5,dataativacao)) and month(ccc.datareferencia) = month(dateadd(month,5,dataativacao)) and datareferencia = eomonth(datareferencia) then LimiteCreditoTotal else null end) as 'LimiteM5'
into		#atraso
from		staging.bi_creditocontratocliente ccc with(nolock) 
inner join	#dataativ da on da.ClientId = ccc.idcliente
where		ccc.datareferencia >= dataativacao
and			dataativacao>='20181201'
and			dataativacao<'20190901'
group by	idcliente

-----------------------------------------------------------------------------------------------------

select		c.id as 'clientid'
			,max(isnull(salario,0)) as 'renda'
into		#renda
from		dbprod02.neonpottencial.dbo.client c with(nolock)
inner join	dbprod02.neonpottencial.dbo.bureauclientedetalhe bcd with(nolock) on c.cpf = bcd.cpf
group by	c.id

-----------------------------------------------------------------------------------------------------

select      distinct (c.ClientId)																																								as 'ClientId'
            ,dc.DataContratacao																																									as 'Data de Contratação do Crédito'
			,da.DataAtivacao																																									as 'Data de Ativação do Crédito'
            ,r.renda																																											as 'Renda Presumida'
			,c.DeclaredIncome																																									as 'Renda Declarada'
			,ccc.IdContaConductorCredito																																						as 'IdContaConductorCredito'
            ,a.AtrasoM0
			,a.DataM0
			,a.LimiteM0
			,a.SaldoM0
			,case when a.AtrasoM1>=5 then 1 else 0 end																																			as 'FPD'
			,AtrasoM1
			,a.DataM1
			,a.LimiteM1
			,a.SaldoM1
			,case when a.AtrasoM2>=30 then 1 else 0 end																																			as 'Over 30 mob 2'
			,AtrasoM2
			,a.DataM2
			,a.LimiteM2
			,a.SaldoM2
            ,case when a.AtrasoM3>=30 then 1 else 0 end																																			as 'Over 30 mob 3'
			,AtrasoM3
			,a.DataM3
			,a.LimiteM3
			,a.SaldoM3
			,case when a.AtrasoM4>=30 then 1 else 0 end																																			as 'Over 30 mob 4'
			,AtrasoM4
			,a.DataM4
			,a.LimiteM4
			,a.SaldoM4
			,case when a.AtrasoM5>=30 then 1 else 0 end																																			as 'Over 30 mob 5'
			,AtrasoM5
			,a.DataM5
			,a.LimiteM5
			,a.SaldoM5
			,gh.GH
into		#base
from        #datacont																																											as dc
inner join	#dataativ																																											as da on dc.ClientId = da.ClientId
inner join	Dimension.Client																																									as c WITH(NOLOCK) on c.ClientId = dc.ClientId
inner join  #atraso																																												as a on a.ClientId = dc.ClientId
left join	dbprod02.neonpottencial.dbo.creditocontratocliente																																	as ccc with(nolock) on ccc.IdCliente = dc.ClientId
left join	#renda																																												as r with(nolock) on r.clientid = dc.ClientId
inner join	analytics_db.dbo.gh_velho																																							as gh with(nolock) on gh.IdContaConductorCredito = ccc.IdContaConductorCredito
where		PersonType = 'PF'
and         FlagFraud = 0
and         CancellationRequestDate is null
and         EndDate is null
and			FlagRegisterDeleted = 0